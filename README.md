# ABSTRACT HASKELL NONSENSE 

## Stuff here is for my Mandelbrot Set generator implemented in Haskell

* It compiles
* It runs
* It's done via recursion 

### BUILDING

```
$ ghc mandel4
```

### RUNNING

To run a compiled copy...you just run it.

```
$ ./mandel4
```

To run an interpreted copy...you load it, then run it:

```
$ ghci
Prelude> :l mandel4
`*Main> main
```

## TODO

* Everything
