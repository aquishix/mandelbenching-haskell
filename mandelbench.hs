import Control.Monad (when)

ripOffThe6th :: (t0, t1, t2, t3, t4, t5) -> t5
ripOffThe6th (a, b, c, d, e, f) = f

mandelbrot :: (Double, Double, Double, Double, Int, String) -> (Double, Double, Double, Double, Int, String)
mandelbrot (cre, cim, zre, zim, its, retChar)
    | magn >= 4.0 && its >= 32 = (cre, cim, zre, zim, its, ".")
    | magn >= 4.0 && its >= 16 = (cre, cim, zre, zim, its, "o")
    | magn >= 4.0              = (cre, cim, zre, zim, its, "O")
    | its >= 1000000           = (cre, cim, zre, zim, its, " ")
    | otherwise                = mandelbrot (cre, cim, (zre ^ 2) - (zim ^ 2) + cre, 2 * zre * zim + cim, its + 1, "-")
    where magn = zre ^ 2 + zim ^ 2

loop m n = when (m <= 39)
            (do (loopBody m n)
                loop (m + 1) n)

loop2 n = when (n >= -20)
           (do loop (-40) n
               putStrLn ""
               loop2 (n - 1))

loopBody m n = putStr (ripOffThe6th (mandelbrot ((fromIntegral m) / 40.0 - 0.5, (fromIntegral n) / 20.0, 0.0, 0.0, 1, "this_doesn'\t_matter")))

main = do
 loop2 19
